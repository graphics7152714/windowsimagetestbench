#include <iostream>
#include "lib/image/inc/Image.hpp"
#include "lib/font/inc/Fonts.hpp"

namespace TestResults {
  std::size_t succes = 0;
  std::size_t total = 0;
};

std::size_t Compare(const Graphics::Image& image, const Graphics::Image& refImage) {
  if (image.getWidth() != refImage.getWidth() || image.getHeight() != refImage.getHeight()) {
    return 0;
  }

  std::size_t correctBytes = 0;
  for (std::size_t i = 0; i < (image.getWidth() + 7) / 8 * image.getHeight(); i++) {
    if (image.bitmap[i] == refImage.bitmap[i]) {
      correctBytes++;
    }
  }

  return correctBytes;
}

void Test(void(*fun)(Graphics::Image&), const char* const resultFilename, std::uint16_t width, std::uint16_t height) {
  Graphics::Image refImage(resultFilename);
  Graphics::Image image(width, height);
  fun(image);

  const std::size_t totalBytes = (image.getWidth() + 7) / 8 * image.getHeight();
  std::size_t correctBytes = 0;

  if (refImage.getHeight() != 0 && (correctBytes = Compare(image, refImage)) == totalBytes) {
    TestResults::succes++;
    std::cout << resultFilename << " - success" << std::endl;
  } else {
    std::cout << resultFilename << " - failed (" << (totalBytes - correctBytes) << "/" << totalBytes << ")" << std::endl;
    char buffer[100];
    sprintf(buffer, "test_%s", resultFilename);
    image.Save(buffer);
  }
  TestResults::total++;
}

void TestThinLinesChaotic(Graphics::Image& image) {
  // Corner; multiple directions; 45 degrees
  image.DrawLine(0, 0, 5, 5);
  image.DrawLine(99, 0, 94, 5);
  image.DrawLine(99, 99, 94, 94);
  image.DrawLine(0, 99, 5, 94);

  // Mid; multiple directions; * degrees
  image.DrawLine(10, 10, 20, 15);
  image.DrawLine(10, 30, 15, 20);
  image.DrawLine(45, 55, 10, 50);

  // Mid; all directions; * degrees
  image.DrawLine(55, 10, 70, 15); // SEE
  image.DrawLine(70, 25, 55, 20); // NWW
  image.DrawLine(70, 30, 55, 35); // SWW
  image.DrawLine(55, 45, 70, 40); // NEE

  image.DrawLine(55, 55, 60, 70); // SSW
  image.DrawLine(70, 70, 65, 55); // NNE
  image.DrawLine(80, 55, 75, 70); // SSE
  image.DrawLine(90, 70, 85, 55); // NNW
}

void TestThickLinesDiagonal(Graphics::Image& image) {
  // SW -> NE
  image.DrawLineAngle(10, 10, 135, 30, 1);
  image.DrawLineAngle(10, 20, 135, 30, 2);
  image.DrawLineAngle(10, 31, 135, 30, 3);
  image.DrawLineAngle(10, 43, 135, 30, 4);
  image.DrawLineAngle(10, 56, 135, 30, 5);
  image.DrawLineAngle(10, 70, 135, 30, 6);
  image.DrawLineAngle(10, 85, 135, 30, 7);
  image.DrawLineAngle(10, 101, 135, 30, 8);
  image.DrawLineAngle(10, 118, 135, 30, 9);
  image.DrawLineAngle(10, 136, 135, 30, 10);

  // NW -> SE
  image.DrawLineAngle(40, 21 + 10, 45, 30, 1);
  image.DrawLineAngle(40, 21 + 20, 45, 30, 2);
  image.DrawLineAngle(40, 21 + 31, 45, 30, 3);
  image.DrawLineAngle(40, 21 + 43, 45, 30, 4);
  image.DrawLineAngle(40, 21 + 56, 45, 30, 5);
  image.DrawLineAngle(40, 21 + 70, 45, 30, 6);
  image.DrawLineAngle(40, 21 + 85, 45, 30, 7);
  image.DrawLineAngle(40, 21 + 101, 45, 30, 8);
  image.DrawLineAngle(40, 21 + 118, 45, 30, 9);
  image.DrawLineAngle(40, 21 + 136, 45, 30, 10);

  // NE -> SW
  image.DrawLineAngle(100, 10, 225, 30, 1);
  image.DrawLineAngle(100, 20, 225, 30, 2);
  image.DrawLineAngle(100, 31, 225, 30, 3);
  image.DrawLineAngle(100, 43, 225, 30, 4);
  image.DrawLineAngle(100, 56, 225, 30, 5);
  image.DrawLineAngle(100, 70, 225, 30, 6);
  image.DrawLineAngle(100, 85, 225, 30, 7);
  image.DrawLineAngle(100, 101, 225, 30, 8);
  image.DrawLineAngle(100, 118, 225, 30, 9);
  image.DrawLineAngle(100, 136, 225, 30, 10);

  // SE -> NW
  image.DrawLineAngle(130, 21 + 10, 315, 30, 1);
  image.DrawLineAngle(130, 21 + 20, 315, 30, 2);
  image.DrawLineAngle(130, 21 + 31, 315, 30, 3);
  image.DrawLineAngle(130, 21 + 43, 315, 30, 4);
  image.DrawLineAngle(130, 21 + 56, 315, 30, 5);
  image.DrawLineAngle(130, 21 + 70, 315, 30, 6);
  image.DrawLineAngle(130, 21 + 85, 315, 30, 7);
  image.DrawLineAngle(130, 21 + 101, 315, 30, 8);
  image.DrawLineAngle(130, 21 + 118, 315, 30, 9);
  image.DrawLineAngle(130, 21 + 136, 315, 30, 10);

  // NW
  image.DrawLineAngle(5, 5, 315, 20, 5);
  // NE
  image.DrawLineAngle(195, 5, 45, 20, 5);
  // SE
  image.DrawLineAngle(195, 195, 135, 20, 5);
  // SW
  image.DrawLineAngle(5, 195, 225, 20, 5);
}

void TestThinLinesStraight(Graphics::Image& image) {
  image.DrawLine(0, 10, 0, 99); // Left, vertical
  image.DrawLine(5, 0, 50, 0); // Top, horizontal
  image.DrawLine(0, 99, 99, 99); // Bottom, horizontal

  // Dashes
  image.DrawLine(10, 10, 11, 10);
  image.DrawLine(13, 10, 14, 10);
  image.DrawLine(16, 10, 17, 10);
  image.DrawLine(19, 10, 20, 10);
  image.DrawLine(22, 10, 23, 10);
}

void TestThickLinesStraight(Graphics::Image& image) {
  // Horizontal
  image.DrawLine(10, 10, 50, 10, 1);
  image.DrawLine(10, 13, 50, 13, 2);
  image.DrawLine(10, 17, 50, 17, 3);
  image.DrawLine(10, 22, 50, 22, 4);
  image.DrawLine(10, 28, 50, 28, 5);
  image.DrawLine(10, 35, 50, 35, 6);
  image.DrawLine(10, 43, 50, 43, 7);
  image.DrawLine(10, 52, 50, 52, 8);
  image.DrawLine(10, 62, 50, 62, 9);
  image.DrawLine(10, 73, 50, 73, 10);

  // Vertical
  image.DrawLine(110, 10, 110, 50, 1);
  image.DrawLine(113, 10, 113, 50, 2);
  image.DrawLine(117, 10, 117, 50, 3);
  image.DrawLine(122, 10, 122, 50, 4);
  image.DrawLine(128, 10, 128, 50, 5);
  image.DrawLine(135, 10, 135, 50, 6);
  image.DrawLine(143, 10, 143, 50, 7);
  image.DrawLine(152, 10, 152, 50, 8);
  image.DrawLine(162, 10, 162, 50, 9);
  image.DrawLine(173, 10, 173, 50, 10);
}

void TestThinLinesClock(Graphics::Image& image) {
  image.DrawLineAngle(60, 60, 00.00, 50);
  image.DrawLineAngle(60, 60, 11.25, 50);
  image.DrawLineAngle(60, 60, 22.50, 50);
  image.DrawLineAngle(60, 60, 33.75, 50);
  image.DrawLineAngle(60, 60, 45.00, 50);
  image.DrawLineAngle(60, 60, 56.25, 50);
  image.DrawLineAngle(60, 60, 67.50, 50);
  image.DrawLineAngle(60, 60, 78.75, 50);
  image.DrawLineAngle(60, 60, 90.00, 50);
  image.DrawLineAngle(60, 60, 101.25, 50);
  image.DrawLineAngle(60, 60, 112.50, 50);
  image.DrawLineAngle(60, 60, 123.75, 50);
  image.DrawLineAngle(60, 60, 135.00, 50);
  image.DrawLineAngle(60, 60, 146.25, 50);
  image.DrawLineAngle(60, 60, 157.50, 50);
  image.DrawLineAngle(60, 60, 168.75, 50);
  image.DrawLineAngle(60, 60, 180.00, 50);
  image.DrawLineAngle(60, 60, 191.25, 50);
  image.DrawLineAngle(60, 60, 202.50, 50);
  image.DrawLineAngle(60, 60, 213.75, 50);
  image.DrawLineAngle(60, 60, 225.00, 50);
  image.DrawLineAngle(60, 60, 236.25, 50);
  image.DrawLineAngle(60, 60, 247.50, 50);
  image.DrawLineAngle(60, 60, 258.75, 50);
  image.DrawLineAngle(60, 60, 270.00, 50);
  image.DrawLineAngle(60, 60, 281.25, 50);
  image.DrawLineAngle(60, 60, 292.50, 50);
  image.DrawLineAngle(60, 60, 303.75, 50);
  image.DrawLineAngle(60, 60, 315.00, 50);
  image.DrawLineAngle(60, 60, 326.25, 50);
  image.DrawLineAngle(60, 60, 337.50, 50);
  image.DrawLineAngle(60, 60, 348.75, 50);

  image.DrawLineAngle(180, 60, 00.00, 50, 2);
  image.DrawLineAngle(180, 60, 11.25, 50, 2);
  image.DrawLineAngle(180, 60, 22.50, 50, 2);
  image.DrawLineAngle(180, 60, 33.75, 50, 2);
  image.DrawLineAngle(180, 60, 45.00, 50, 2);
  image.DrawLineAngle(180, 60, 56.25, 50, 2);
  image.DrawLineAngle(180, 60, 67.50, 50, 2);
  image.DrawLineAngle(180, 60, 78.75, 50, 2);
  image.DrawLineAngle(180, 60, 90.00, 50, 2);
  image.DrawLineAngle(180, 60, 101.25, 50, 2);
  image.DrawLineAngle(180, 60, 112.50, 50, 2);
  image.DrawLineAngle(180, 60, 123.75, 50, 2);
  image.DrawLineAngle(180, 60, 135.00, 50, 2);
  image.DrawLineAngle(180, 60, 146.25, 50, 2);
  image.DrawLineAngle(180, 60, 157.50, 50, 2);
  image.DrawLineAngle(180, 60, 168.75, 50, 2);
  image.DrawLineAngle(180, 60, 180.00, 50, 2);
  image.DrawLineAngle(180, 60, 191.25, 50, 2);
  image.DrawLineAngle(180, 60, 202.50, 50, 2);
  image.DrawLineAngle(180, 60, 213.75, 50, 2);
  image.DrawLineAngle(180, 60, 225.00, 50, 2);
  image.DrawLineAngle(180, 60, 236.25, 50, 2);
  image.DrawLineAngle(180, 60, 247.50, 50, 2);
  image.DrawLineAngle(180, 60, 258.75, 50, 2);
  image.DrawLineAngle(180, 60, 270.00, 50, 2);
  image.DrawLineAngle(180, 60, 281.25, 50, 2);
  image.DrawLineAngle(180, 60, 292.50, 50, 2);
  image.DrawLineAngle(180, 60, 303.75, 50, 2);
  image.DrawLineAngle(180, 60, 315.00, 50, 2);
  image.DrawLineAngle(180, 60, 326.25, 50, 2);
  image.DrawLineAngle(180, 60, 337.50, 50, 2);
  image.DrawLineAngle(180, 60, 348.75, 50, 2);

  image.DrawLineAngle(60, 180, 11.25, 50, 3);
  image.DrawLineAngle(60, 180, 33.75, 50, 3);
  image.DrawLineAngle(60, 180, 56.25, 50, 3);
  image.DrawLineAngle(60, 180, 78.75, 50, 3);
  image.DrawLineAngle(60, 180, 101.25, 50, 3);
  image.DrawLineAngle(60, 180, 123.75, 50, 3);
  image.DrawLineAngle(60, 180, 146.25, 50, 3);
  image.DrawLineAngle(60, 180, 168.75, 50, 3);
  image.DrawLineAngle(60, 180, 191.25, 50, 3);
  image.DrawLineAngle(60, 180, 213.75, 50, 3);
  image.DrawLineAngle(60, 180, 236.25, 50, 3);
  image.DrawLineAngle(60, 180, 258.75, 50, 3);
  image.DrawLineAngle(60, 180, 281.25, 50, 3);
  image.DrawLineAngle(60, 180, 303.75, 50, 3);
  image.DrawLineAngle(60, 180, 326.25, 50, 3);
  image.DrawLineAngle(60, 180, 348.75, 50, 3);

  image.DrawLineAngle(180, 180, 11.25, 50, 4);
  image.DrawLineAngle(180, 180, 33.75, 50, 4);
  image.DrawLineAngle(180, 180, 56.25, 50, 4);
  image.DrawLineAngle(180, 180, 78.75, 50, 4);
  image.DrawLineAngle(180, 180, 101.25, 50, 4);
  image.DrawLineAngle(180, 180, 123.75, 50, 4);
  image.DrawLineAngle(180, 180, 146.25, 50, 4);
  image.DrawLineAngle(180, 180, 168.75, 50, 4);
  image.DrawLineAngle(180, 180, 191.25, 50, 4);
  image.DrawLineAngle(180, 180, 213.75, 50, 4);
  image.DrawLineAngle(180, 180, 236.25, 50, 4);
  image.DrawLineAngle(180, 180, 258.75, 50, 4);
  image.DrawLineAngle(180, 180, 281.25, 50, 4);
  image.DrawLineAngle(180, 180, 303.75, 50, 4);
  image.DrawLineAngle(180, 180, 326.25, 50, 4);
  image.DrawLineAngle(180, 180, 348.75, 50, 4);
}

void TestHouseOfNicholas(Graphics::Image& image) {
  //      _  /_  /_\ /_\ /_\ /_\
  // _ _| _|  _|  _|  /| |/| |X|
  const std::uint16_t X[] = { 10, 60, 60, 10, 35, 60, 10, 10, 60 };
  const std::uint16_t Y[] = { 85, 85, 35, 35, 10, 35, 85, 35, 85 };

  for (std::size_t y = 0; y < 3; y++) {
    for (std::size_t x = 0; x < 3; x++) {
      std::uint16_t X2[9];
      std::uint16_t Y2[9];
      for (std::size_t i = 0; i < 9; i++) {
        X2[i] = X[i] + 60 * x;
        Y2[i] = Y[i] + 85 * y;
      }
      image.DrawPath(X2, Y2, 9, y * 3 + x + 1);
    }
  }
}

int main() {
  std::cout << "Hello World" << std::endl;

  Test(TestThinLinesStraight, "ThinLinesStraight.bmp", 100, 100);
  //Test(TestThinLinesChaotic, "ThinLinesChaotic.bmp", 100, 100);
  Test(TestThickLinesDiagonal, "ThickLinesDiagonal.bmp", 200, 200);
  Test(TestThickLinesStraight, "ThickLinesStraight.bmp", 200, 200);
  Test(TestThinLinesClock, "ThinLinesClock.bmp", 300, 300);
  Test(TestHouseOfNicholas, "HouseOfNicholas.bmp", 190, 275);

  std::cout << "Success: " << TestResults::succes << " / Total: " << TestResults::total << std::endl;
  if (TestResults::succes == TestResults::total) {
    std::cout << "All tests passed" << std::endl;
  } else {
    std::cout << TestResults::total - TestResults::succes << " tests failed" << std::endl;
  }

  return 0;
}