/**
 * @file File_.hpp
 * @author Julian Neundorf
 * @brief File access
 * @version 0.1
 * @date 2024-06-10
 *
 * Remarks:
*/
#pragma once

#include <stdio.h>
#include <cstdint>
#include <iostream>
#include <memory>

class File_ {
public:
  ~File_() {
    if (file_ != nullptr) {
      free(file_);
    }
  }

  bool open(const char* const filename, const char* const mode) {
    return (file_ = fopen(filename, mode)) != nullptr;
  }

  size_t read(void* _DstBuf, size_t _ElementSize, size_t _Count) {
    return fread(_DstBuf, _ElementSize, _Count, file_);
  }

  int seek(long _Offset, int _Origin) {
    return fseek(file_, _Offset, _Origin);
  }

  int getc() {
    return fgetc(file_);
  }

  int putc(int ch) {
    return fputc(ch, file_);
  }

  int close() {
    return fclose(file_);
  }

private:
  FILE* file_ = nullptr;
};