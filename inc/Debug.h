/**
 * @file Debug.h
 * @author Julian Neundorf (julian9dorf@gmail.com)
 * @brief Configuration for shown debugging information
 * @version 1.0
 * @date 2022-05-08
 *
 * Remarks:
*/
#pragma once

#include "stdint.h"
#include "stdlib.h"

#define DEBUG_DEPTH_NONE    0
#define DEBUG_DEPTH_INFO    1
#define DEBUG_DEPTH_DEEP    2

#define DEBUG_DEBUG         DEBUG_DEPTH_DEEP //DEBUG_DEPTH_NONE or DEBUG_DEPTH_INFO or DEBUG_DEPTH_DEEP
#define DEBUG_ERROR         DEBUG_DEPTH_DEEP

#define D_MAIN_DEBUG_LIST                        0
#define D_OWIFI_HANDLEROOT                       1
#define D_CREDENTIALS_CREDENTIALS                2
#define D_CREDENTIALS_SAVE                       3
#define D_GUI_WAITUNTILIDE                       4
#define D_IMAGE_MERGE                            5
#define D_IMAGE_DRAWCIRCLEPART                   6
#define D_IMAGE_DRAWCHAR                         7
#define D_LATEX_INTERPRET                        8
#define D_WEATHER_WEATHER                        9
#define D_WEATHER_PRINT_WEATHER                  10
#define D_WEATHER_PRINT_GRAPH                    11
#define D_CALC_CALCULATION                       12
#define D_CALC_CALCULATE                         13
#define D_CALC_CPYSTRING                         14
#define D_CALC_CALCULATE_FORMULA                 15
#define D_MAIN_PRINTSCREEN                       16
#define D_IMAGE_SCREENINIT                       17
#define D_OWIFI_WIFI                             18
#define D_OWIFI_HANDLE_SAVECONFIGURATIONVARIABLE 19
#define D_OWIFI_HANDLE_CALCULATELATEX            20
#define D_LATEX_PRINT_TEX                        21
#define D_IMAGE_IMAGE                            22
#define D_IMAGE_MIRROR                           23
#define D_IMAGE_ROTATE                           24
#define D_PRESENTATION_TYPES                     25
#define D_CALCULATOR_CALCULATE_RUNNER            26
#define D_CALCULATOR_CDVO                        27
#define D_CALCULATOR_NUMBER                      28

const uint8_t DEBUGGING_LIST[] = {
  /* D_MAIN_DEBUG_LIST                        -> */ DEBUG_DEPTH_NONE,
  /* D_OWIFI_HANDLEROOT                       -> */ DEBUG_DEPTH_NONE,
  /* D_CREDENTIALS_CREDENTIALS                -> */ DEBUG_DEPTH_NONE,
  /* D_CREDENTIALS_SAVE                       -> */ DEBUG_DEPTH_NONE,
  /* D_GUI_WAITUNTILIDE                       -> */ DEBUG_DEPTH_NONE,
  /* D_IMAGE_MERGE                            -> */ DEBUG_DEPTH_NONE,
  /* D_IMAGE_DRAWCIRCLEPART                   -> */ DEBUG_DEPTH_NONE,
  /* D_IMAGE_DRAWCHAR                         -> */ DEBUG_DEPTH_NONE,
  /* D_LATEX_INTERPRET                        -> */ DEBUG_DEPTH_NONE,
  /* D_WEATHER_WEATHER                        -> */ DEBUG_DEPTH_NONE,
  /* D_WEATHER_PRINT_WEATHER                  -> */ DEBUG_DEPTH_NONE,
  /* D_WEATHER_PRINT_GRAPH                    -> */ DEBUG_DEPTH_NONE,
  /* D_CALC_CALCULATION                       -> */ DEBUG_DEPTH_NONE,
  /* D_CALC_CALCULATE                         -> */ DEBUG_DEPTH_NONE,
  /* D_CALC_CPYSTRING                         -> */ DEBUG_DEPTH_NONE,
  /* D_CALC_CALCULATE_FORMULA                 -> */ DEBUG_DEPTH_NONE,
  /* D_MAIN_PRINTSCREEN                       -> */ DEBUG_DEPTH_DEEP,
  /* D_IMAGE_SCREENINIT                       -> */ DEBUG_DEPTH_NONE,
  /* D_OWIFI_WIFI                             -> */ DEBUG_DEPTH_NONE,
  /* D_OWIFI_HANDLE_SAVECONFIGURATIONVARIABLE -> */ DEBUG_DEPTH_NONE,
  /* D_OWIFI_HANDLE_CALCULATELATEX            -> */ DEBUG_DEPTH_NONE,
  /* D_LATEX_PRINT_TEX                        -> */ DEBUG_DEPTH_NONE,
  /* D_IMAGE_IMAGE                            -> */ DEBUG_DEPTH_NONE,
  /* D_IMAGE_MIRROR                           -> */ DEBUG_DEPTH_NONE,
  /* D_IMAGE_ROTATE                           -> */ DEBUG_DEPTH_NONE,
  /* D_PRESENTATION_TYPES                     -> */ DEBUG_DEPTH_NONE,
  /* D_CALCULATOR_CALCULATE_RUNNER            -> */ DEBUG_DEPTH_NONE,
  /* D_CALCULATOR_CDVO                        -> */ DEBUG_DEPTH_NONE,
  /* D_CALCULATOR_NUMBER                      -> */ DEBUG_DEPTH_NONE,
};

//#define DEBUG_OUTPUT Serial.
#define DEBUG_OUTPUT

#define HEAP_CHECK(POS)     { if (!heap_caps_check_integrity_all(true)) if (DEBUG_ERROR) DEBUG_OUTPUT printf("Heap-Fehler an Position: %s\n", POS); }
#define DEBUG(index, type)  (DEBUGGING_LIST[index] >= type)
